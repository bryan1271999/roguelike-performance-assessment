﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public static bool jumping = false;
    public static float playerY;
    public static float playerX;
    private bool isSpeed = false;
    public static int playerHealth = 50;
    public GameObject[] bullet;
    public static bool smoothFalling = false;
    private Transform enemyTransform;
    private bool enemyJumping = false;
    private Collider2D enemyCollider;
    public AudioSource shoot;


    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Floor" || collider.tag == "Platform")
        {
            smoothFalling = false;
            jumping = false;
        }
        if (collider.tag == "Speed")
        {
            isSpeed = true;
            Invoke("IsSpeedFalse", 10);
            collider.gameObject.SetActive(false);
        }
        if (collider.tag == "Enemy" || collider.tag == "EnemyBullet")
        {
            enemyCollider = collider.GetComponent<Collider2D>();
            enemyTransform = collider.GetComponent<Transform>();
            playerHealth -= 5;
            Animator animator = GetComponent<Animator>();
            animator.CrossFade("player-hurt", 0);
        }
        if (collider.tag == "Lava")
        {
            var gameController = new GameController();
            gameController.GameOver();
        }
    }

    private void PlayerAttack()
    {
        Animator animator = GetComponent<Animator>();
        animator.CrossFade("player-attack", 0);
                if (Input.GetKey(KeyCode.A))
                {
                    Instantiate(bullet[1], new Vector3(playerX, playerY, transform.position.z), Quaternion.identity);
                    BulletLeft.isShootingLeft = true;
                    Bullet.isShootingRight = false;
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    Instantiate(bullet[0], new Vector3(playerX, playerY, transform.position.z), Quaternion.identity);
                    Bullet.isShootingRight = true;
                    BulletLeft.isShootingLeft = false;
                }
    }

    private void IsSpeedFalse()
    {
        isSpeed = false;
    }

    void Update() {
        if (GameController.isStart)
        {
            playerY = transform.position.y;
            playerX = transform.position.x;
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow))
            {
                MovePlayer();
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                JumpPlayer();
            }
            if (playerHealth <= 0)
            {
                var gameController = new GameController();
                gameController.GameOver();
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
            {
                shoot.Play();
                PlayerAttack();
            }
            if (smoothFalling)
            {
                transform.Translate(0, -0.1f, 0);
            }
            if (enemyCollider != null)
            {
                if (enemyCollider.tag == "Floor" || enemyCollider.tag == "Platform")
                {
                    enemyJumping = false;
                    smoothFalling = false;
                }
            }
            if (enemyTransform != null)
            {
                if (transform.position.x - enemyTransform.position.x <= 0.5f && transform.position.x - enemyTransform.position.x >= -0.5f)
                {
                    if (transform.position.y - enemyTransform.position.y < -1)
                    {
                        if (enemyJumping == false)
                        {
                            playerHealth -= 10;
                            Invoke("EnemyJump", 0.02f);
                            Invoke("EnemyJump", 0.04f);
                            Invoke("EnemyJump", 0.06f);
                            Invoke("EnemyJump", 0.08f);
                            Invoke("EnemyJump", 0.1f);
                            Invoke("EnemyJump", 0.12f);
                            Invoke("EnemyJump", 0.14f);
                            Invoke("EnemyJump", 0.16f);
                            Invoke("EnemyJump", 0.18f);
                        }
                    }
                }
            }
        }
    }

    private void EnemyJump()
    {
        enemyTransform.Translate(0, 0.5f, 0);
        enemyJumping = true;
    }

    private void MovePlayer()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if(isSpeed)
            {
                transform.Translate(0.5f, 0, 0);
            }
            else
            {
                transform.Translate(0.3f, 0, 0);
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (isSpeed)
            {
                transform.Translate(-0.5f, 0, 0);
            }
            else
            {
                transform.Translate(-0.3f, 0, 0);
            }
        }
    }

    private void IncrementUp()
    {
        transform.Translate(0, 0.5f, 0);
        jumping = true;
    }

    private void JumpPlayer()
    {
        if(jumping == false)
        {
            Invoke("IncrementUp", 0.02f);
            Invoke("IncrementUp", 0.04f);
            Invoke("IncrementUp", 0.06f);
            Invoke("IncrementUp", 0.08f);
            Invoke("IncrementUp", 0.1f);
            Invoke("IncrementUp", 0.12f);
            Invoke("IncrementUp", 0.14f);
            Invoke("IncrementUp", 0.16f);
            Invoke("IncrementUp", 0.18f);
            jumping = true;
        }
    }



}
