﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
    private Animator animator;

	void Start () {
        animator = GetComponent<Animator>();
	}
	
	void Update () {
        if(Player.playerHealth == 45)
        {
            animator.SetTrigger("nine");
        }
        else if(Player.playerHealth == 40)
        {
            animator.SetTrigger("eight");
        }
        else if(Player.playerHealth == 35)
        {
            animator.SetTrigger("seven");
        }
        else if(Player.playerHealth == 30)
        {
            animator.SetTrigger("six");
        }
        else if(Player.playerHealth == 25)
        {
            animator.SetTrigger("five");
        }
        else if(Player.playerHealth == 20)
        {
            animator.SetTrigger("four");
        }
        else if(Player.playerHealth == 15)
        {
            animator.SetTrigger("three");
        }
        else if(Player.playerHealth == 10)
        {
            animator.SetTrigger("two");
        }
        else if(Player.playerHealth == 5)
        {
            animator.SetTrigger("one");
        }
	}
}
