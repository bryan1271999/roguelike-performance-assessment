﻿using UnityEngine;
using System.Collections;

public class EnemyBulletLeft : MonoBehaviour
{
    public static bool isShootingLeft = false;

    void Start()
    {
        Invoke("DeleteBullet", 1.5f);
    }

    private void DeleteBullet()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (isShootingLeft)
        {
            transform.Translate(-0.5f, 0, 0);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            isShootingLeft = false;
            gameObject.SetActive(false);
        }
        if (collider.tag == "Wall" || collider.tag == "Platform")
        {
            isShootingLeft = false;
            gameObject.SetActive(false);
        }
    }
}

