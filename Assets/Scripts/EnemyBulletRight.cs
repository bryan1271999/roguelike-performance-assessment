﻿using UnityEngine;
using System.Collections;

public class EnemyBulletRight : MonoBehaviour {
    public static bool isShootingRight = false;

    void Start()
    {
        Invoke("DeleteBullet", 1.5f);
    }

    private void DeleteBullet()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (isShootingRight)
        {
            transform.Translate(0.5f, 0, 0);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            isShootingRight = false;
            gameObject.SetActive(false);
        }
        if (collider.tag == "Wall" || collider.tag == "Platform")
        {
            isShootingRight = false;
            DeleteBullet();
        }
    }
}
