﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public GameObject[] platforms;
    public GameObject[] lava;
    public GameObject[] powerUps;
    public GameObject[] enemies;
    public AudioSource music;
    public static bool isStart = false;
    public static bool easy;
    public static bool medium;
    public static bool hard;
    public Text menu;

    void Start() {

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && isStart == false)
        {
            menu.text = "Difficulty: 1, 2, 3";
        }
        else if (menu.text == "Difficulty: 1, 2, 3" && Input.GetKey(KeyCode.Alpha1))
        {
            easy = true;
            menu.text = "";
            StartGame();
        }
        else if(menu.text == "Difficulty: 1, 2, 3" && Input.GetKey(KeyCode.Alpha2))
        {
            medium = true;
            menu.text = "";
            StartGame();
        }
        else if (menu.text == "Difficulty: 1, 2, 3" && Input.GetKey(KeyCode.Alpha3))
        {
            hard = true;
            menu.text = "";
            StartGame();
        }
    }

    public void StartGame()
    {
        AddPlatformsLevel1();
        isStart = true;
    }

    public void GameOver()
    {
        GameObject player = GameObject.Find("Player");
        player.SetActive(false);
    }

    public void AddPlatformsLevel1()
    {
        Instantiate(platforms[0], new Vector3(-100, 17, 0), Quaternion.identity);
        Instantiate(platforms[0], new Vector3(-90, 17, 0), Quaternion.identity);
        Instantiate(platforms[0], new Vector3(20, 17, 0), Quaternion.identity);
        Instantiate(platforms[0], new Vector3(26.7f, 19f, 0), Quaternion.identity);
        Instantiate(lava[0], new Vector3(-95, 17f, 0), Quaternion.identity);
        Instantiate(powerUps[0], new Vector3(-80, 16.3f,0), Quaternion.identity);
        Instantiate(enemies[0], new Vector3(-60, 17f, 0), Quaternion.identity);
        Instantiate(enemies[0], new Vector3(-20, 17f, 0), Quaternion.identity);
        Instantiate(enemies[0], new Vector3(0, 17f, 0), Quaternion.identity);
    }
}
