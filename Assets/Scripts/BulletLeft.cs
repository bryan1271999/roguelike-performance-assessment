﻿using UnityEngine;
using System.Collections;

public class BulletLeft : MonoBehaviour {
    public static bool isShootingLeft = false;
    private int stopBullet = 0;

    void Start()
    {
        Invoke("DeleteBullet", 1.5f);
    }

    private void DeleteBullet()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
            if (isShootingLeft)
            {
                stopBullet++; 
            if(stopBullet == 20)
            {
                gameObject.SetActive(false);
            }
            else
            {
                transform.Translate(-2, 0, 0);
            }
        }
            else
        {
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Enemy")
        {
            isShootingLeft = false;
            gameObject.SetActive(false);
        }
        if (collider.tag == "Wall" || collider.tag == "Platform")
        {
            isShootingLeft = false;
            gameObject.SetActive(false);
        }
    }
}
