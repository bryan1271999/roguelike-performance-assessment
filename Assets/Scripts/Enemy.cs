﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    public float enemyHealth;
    private int bulletCount;
    public GameObject[] bullets;
    private float enemyX;
    private float enemyY;
    private double shootLeft;
    private double shootRight;
    private double jump;
    private bool jumping;
    private bool dieOnce = false;
    private bool turn = true;

	void Start () {

	}
	
	void Update () {
        enemyX = transform.position.x;
        enemyY = transform.position.y;
        jump++;
        if(jump >= 300)
        {
            jump = 0;
            JumpEnemy();
        }
        if (transform.position.x - Player.playerX < 20 && turn)
        {
            if (transform.position.x - Player.playerX > 0)
            {
                if(GameController.easy) {
                    shootLeft += 0.5;
                }
                else if (GameController.medium)
                {
                    shootLeft += 1;
                }
                else if(GameController.hard)
                {
                    shootLeft += 3;
                }
                if (shootLeft >= 100)
                {
                    shootLeft = 0;
                    ShootLeft();
                }
                transform.localEulerAngles = new Vector3(0, 180, 0);
                transform.Translate(0.1f, 0, 0);
            }
            else if (transform.position.x - Player.playerX < 0)
            {
                if(GameController.easy)
                {
                    shootRight += 0.5;
                }
                else if(GameController.medium)
                {
                    shootRight += 1;
                }
                else if(GameController.hard)
                {
                    shootRight += 3;
                }
                if (shootRight >= 100)
                {
                    shootRight = 0;
                    ShootRight();
                }
                transform.localEulerAngles = new Vector3(0, 0, 0);
                transform.Translate(0.1f, 0, 0);
            }
            if (enemyHealth <= 0)
            {
                gameObject.SetActive(false);
            }
        }
        if (transform.position.x - Player.playerX <= 0.5f && transform.position.x - Player.playerX >= -0.5f && turn)
        {
            if(transform.position.y - Player.playerY >= -3)
            {
                Player.smoothFalling = true;
                turn = false;
                Invoke("SqueezeDown", 0.05f);
                Invoke("SqueezeDown", 0.1f);
                Invoke("SqueezeDown", 0.15f);
                Invoke("SqueezeDown", 0.2f);
                Invoke("SqueezeDown", 0.25f);
                Invoke("SqueezeDown", 0.3f);
                Invoke("EnemyDie", 0.4f);
            }
        }
	}

    private void SqueezeDown()
    {
        transform.Rotate(-15, 0, 0);
    }

    private void JumpEnemy()
    {
        if(jumping == false)
        {
            Invoke("IncrementUp", 0.02f);
            Invoke("IncrementUp", 0.04f);
            Invoke("IncrementUp", 0.06f);
            Invoke("IncrementUp", 0.08f);
            Invoke("IncrementUp", 0.1f);
            Invoke("IncrementUp", 0.12f);
            Invoke("IncrementUp", 0.14f);
            Invoke("IncrementUp", 0.16f);
            Invoke("IncrementUp", 0.18f);
            jumping = true;
        }
    }

    private void IncrementUp()
    {
        transform.Translate(0, 0.5f, 0);
        jumping = true;
    }

    private void ShootLeft()
    {
        Animator animator = GetComponent<Animator>();
        animator.CrossFade("enemy-attack", 0);
        Instantiate(bullets[1], new Vector3(enemyX, enemyY, 0), Quaternion.identity);
        EnemyBulletLeft.isShootingLeft = true;
    }

    private void ShootRight()
    {
        Animator animator = GetComponent<Animator>();
        animator.CrossFade("enemy-attack", 0);
        Instantiate(bullets[0], new Vector3(enemyX, enemyY, 0), Quaternion.identity);
        EnemyBulletRight.isShootingRight = true;
    }

    private void EnemyDie()
    {
        gameObject.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Bullet")
        {
            bulletCount++;
            if(bulletCount >= 100)
            {
                if(dieOnce == false)
                {
                    jumping = true;
                    Animator animator = GetComponent<Animator>();
                    animator.CrossFade("enemy-die", 0);
                    Invoke("EnemyDie", 0.5f);
                    dieOnce = true;
                }
            }
        }
        if(collider.tag == "Floor")
        {
            jumping = false;
        }
    }

    public void LoseHealth()
    {
        enemyHealth -= 0.1f;
    }
}
