﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    public static bool isShootingRight = false;
    private int stopBullet;

	void Start () {
        Invoke("DeleteBullet", 1.5f);
	}

    private void DeleteBullet()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (isShootingRight)
        {
            stopBullet++;
            if (stopBullet == 20)
            {
                gameObject.SetActive(false);
            }
            else
            {
                transform.Translate(2, 0, 0);
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
    
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Enemy")
        {
            isShootingRight = false;
            gameObject.SetActive(false);
        }
        if(collider.tag == "Wall" || collider.tag == "Platform")
        {
            isShootingRight = false;
            DeleteBullet();
        }
    }
}
